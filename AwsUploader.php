<?php

namespace lib\AwsUploader;

use Aws\S3\S3Client;
use Nette\Utils\ArrayHash;
use Nette\Utils\Image;
use Tracy\Debugger;

class AwsUploader {

	const S3_BUCKET_KEY = 's3_bucket';
	const AWS_ID = 'aws_key';
	const AWS_SECRET = 'aws_secret';


	protected $config;

	/** @var  S3Client */
	protected $client;


	protected $thumbnailConstraints = [];


	public function __construct(S3Client $client)
	{
		$this->client = $client;
	}


	public function setConfig($config)
	{
		$this->config = $config;
	}


	// set thumbnails constraints
	public function setThumbnailConstraints($constraints)
	{
		$this->thumbnailConstraints = $constraints;
	}



	public function upload($filename, $file, $perm = 'public-read', $bucket = NULL, $other = [])
	{
		if(!$bucket) {
			$bucket = $this->getBucketName();
		}

		if(!array_key_exists('Content-Type', $other))
		{
			$other['Content-Type'] = 'image/jpeg';
		}

		// $upload = $this->client->upload($bucket, $filename, $file, $perm, $other);

		$upload = $this->client->putObject([
			'Bucket'                => $bucket,
			'Key'                   => $filename,
			'ContentType'           => $other['Content-Type'],
			'SourceFile'            => $file,
			'ACL'                   => $perm
		]);

		return $upload->get('ObjectURL');
	}


	public function uploadResource($filename, $file, $perm = 'public-read', $bucket = NULL, $other = [])
	{
		if(!$bucket) {
			$bucket = $this->getBucketName();
		}

		$upload = $this->client->upload($bucket, $filename, $file, $perm, $other);

		Debugger::barDump($upload);

		return $upload->get('ObjectURL');
	}


	public function uploadWithThumbnails($filename, $file, $directory = "", $perm = 'public-read', $bucket = NULL, $other = [])
	{
		if(!$bucket) {
			$bucket = $this->getBucketName();
		}

		if(!array_key_exists('Content-Type', $other))
		{
			$other['Content-Type'] = 'image/jpeg';
		}

		if(!empty($directory))
		{
			$directory = rtrim($directory, '/') . '/';
		}

		$explodedFilename = explode('.', $filename);
		$ext = array_pop($explodedFilename);
		$filenameWithoutExtension = implode('.', $explodedFilename);
		$unique = $this->getUniqueHash();


		$result = [];

		$watermark = Image::fromFile(__DIR__ . '/watermark.png');

		// generate thumbnails
		foreach($this->thumbnailConstraints as $constraint)
		{
			list($width, $height) = $constraint;

			$info = @getimagesize($file);

			$image = Image::fromFile($file);
			$image->resize($width, $height);

			Debugger::barDump($image, 'im');

			try {
					$image->place($watermark,
						$image->getWidth() - $watermark->getWidth() - 10,
						$image->getHeight() - $watermark->getHeight() - 10
					);
			} catch(\Exception $e)
			{
				Debugger::barDump($e->getMessage(), 'w');
			}


			$temp = tempnam(sys_get_temp_dir(), '');

			$type = $info[2];
			if($other['Content-Type'] === 'image/gif')
			{
				$type = Image::JPEG;
				$ext = 'jpg';
			}

			$image->save($temp, NULL, $type);

			$thumbname = sprintf("{$unique}-{$filenameWithoutExtension}_%dx%d.{$ext}", $width, $height);

			$thumbnailURL = $this->upload("{$directory}{$thumbname}", $temp, $perm, $bucket, $other);

			$thumbMeta = new ArrayHash();
			$thumbMeta['url'] = "http://nt-media.nerfteemo.com/{$directory}{$thumbname}";
			$thumbMeta['width'] = $width;
			$thumbMeta['height'] = $height;
			$result['thumbnails'][] = $thumbMeta;
		}


			$this->upload("{$directory}$unique-$filename", $file, $perm, $bucket, $other);
			$result['image'] = "http://nt-media.nerfteemo.com/{$directory}$unique-$filename";
			return $result;

	}


	protected function getUniqueHash()
	{
		return uniqid();
	}



	public function getBucketName()
	{
		if(array_key_exists(self::S3_BUCKET_KEY, $this->config))
		{
			return $this->config[self::S3_BUCKET_KEY];
		}

		throw new \Exception("Undefined s3_bucket variable.");
	}


	public function __set($name, $value)
	{
		$this->config[$name] = $value;
	}


	public function __get($name)
	{
		if(array_key_exists($name, $this->config))
		{
			return $this->config[$name];
		}

		throw new \Exception(__CLASS__ . ": Undefined config variable '$name'");
	}

} 